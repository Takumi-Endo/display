// リングバッファ
var RingBuffer = function (bufferCount) {
  if (bufferCount === undefined)
    bufferCount = 0;
  this.buffer = new Array(bufferCount);
  this.count = 0; // 追加データ数
};
RingBuffer.prototype =
{
  // データ追加
  // 除去（上書き）されたデータ数を返す
  add: function (data) {
    lastIndex = (this.count % this.buffer.length);
    // リングバッファの最後尾にデータ追加
    this.buffer[lastIndex] = data;
    this.count++;
    return (this.count <= this.buffer.length ? 0 : 1);
  },
  // データ取得
  get: function (index) {
    if (this.buffer.length < this.count)
      index += this.count;
    index %= this.buffer.length;
    return this.buffer[index];
  },
  // データ数取得
  // バッファのデータ数と追加データ数の小さい方を返す
  getCount: function () {
    return Math.min(this.buffer.length, this.count);
  },

  /* 0:正常, 1:要素不足 */
  getSum:function(sumrange){
    var sum = 0;
    var indexstart = 0;
    var lastindex = (this.count % this.buffer.length);
    var loopend=0;
    var counter = 0;
    //console.log("現在のCT："+this.count);
    
    if(this.count < this.buffer.length){//this.countがAltRingBfSize-1まで
      if(sumrange < this.count){//this.countがAltAveRange+1から
        indexstart = lastindex - 1;
        loopend = (lastindex - sumrange);
      }else{//初めのthis.countがAltAveRangeまで
        indexstart = this.count - 1;
        loopend = 0;
      }
      counter = 0;

      for(var i = indexstart ; i >= loopend ; i--){
        sum += this.buffer[i];
        counter++;
        // console.log("1idx:"+this.buffer[i]+" @"+i);
      }
      
    }else{//リングバッファのthis.countが20以上
      counter = 0;
      indexstart = lastindex -1;

      if(lastindex >= sumrange){//lastindexがsumrangeの数以上
        loopend = lastindex - sumrange;
        for(var i = indexstart ; i >= loopend ; i--){
          sum += this.buffer[i];
          counter++;
          // console.log("2idx:"+this.buffer[i]+" @"+i);
        }
      }else{//lastindexがsumrangeの数以下：[0]と[AltRingBfSize-1]をまたぐ場合
        //[lastindex] -> [0]
        loopend = 0;
        for(var i = indexstart ; i >= loopend ; i--){
          sum += this.buffer[i];
          counter++;
          // console.log("3idx:"+this.buffer[i]+" @"+i);
        }
        
        //[AltRingBfSize-1] -> [(AltRingBfSize-1)-(sumrange-1-lastindex)]
        indexstart = this.buffer.length - 1;
        loopend = (this.buffer.length - 1) - (sumrange - (lastindex + 1));
        for(var i = indexstart; i >= loopend ; i--){
          sum += this.buffer[i];
          counter++;
          // console.log("4idx:"+this.buffer[i]+" @"+i);
        }
      }
    }
    return [counter,sum];
  },

  getAverage: function(averange){//[state]0:shortage, 1:full
    var sum = this.getSum(averange);
    //console.log("Ave:"+sum[1]/sum[0]);
    return sum[1]/sum[0];
  }

};